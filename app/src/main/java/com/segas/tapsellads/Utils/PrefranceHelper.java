package com.segas.tapsellads.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.segas.tapsellads.Model.AdWaterfall;
import com.segas.tapsellads.Model.Waterfall;

import org.json.JSONObject;

//prefrance helper
public class PrefranceHelper {

    Context context;
  static   SharedPreferences prf;
    public  PrefranceHelper(Context context){
        this.context = context;
        prf = context.getSharedPreferences("Tapsell",Context.MODE_PRIVATE);
    }
    public static   void AddWaterFall(AdWaterfall waterfall, long timestamp){
        Gson gson = new Gson();
        waterfall.setWaterfallTime(timestamp);
        String data = gson.toJson(waterfall);
        prf.edit().putString("waterfall",data).apply();
    }

//convert data into json format and save in to shareprefrnce
public static   AdWaterfall GetLastWaterFall(){
    Gson gson = new Gson();
   String data =  prf.getString("waterfall","");
    return gson.fromJson(data,AdWaterfall.class);
}


}
