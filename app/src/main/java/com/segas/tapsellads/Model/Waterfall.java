package com.segas.tapsellads.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
//waterfall parser
public class Waterfall {

    //{ "type": "Rewarded", "waterfall": [ { "name": "UnityAds", "id": "TapsellUnityAdsRewarded" }, { "name": "Tapsell", "id": "5caaf03dc1ed8b000149cedd" } ] }

    @Expose
    @SerializedName("name")
    private  String name;

    @Expose
    @SerializedName("id")
    private  String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
