package com.segas.tapsellads.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ad {

    //{ "adNetworks": [ { "name": "UnityAds", "id": "4112135" }, { "name": "Tapsell", "id": "kttjtpmdehsmnhlkkrlfekisnfifqtdallotfeccaspodsnqspelhcinjjdbiqtmhaglsn" } ] }

    //ad parser for network response
    @Expose
    @SerializedName("name")
    private  String name;

    @Expose
    @SerializedName("id")
    private  String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
