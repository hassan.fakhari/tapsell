package com.segas.tapsellads.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdNetwork {

    //{ "adNetworks": [ { "name": "UnityAds", "id": "4112135" }, { "name": "Tapsell", "id": "kttjtpmdehsmnhlkkrlfekisnfifqtdallotfeccaspodsnqspelhcinjjdbiqtmhaglsn" } ] }

    //ad parser for ad network
    @Expose
    @SerializedName("adNetworks")
    public List<Ad> AdNetwork;

    public List<Ad> getAdNetwork() {
        return AdNetwork;
    }

    public void setAdNetwork(List<Ad> adNetwork) {
        AdNetwork = adNetwork;
    }
}
