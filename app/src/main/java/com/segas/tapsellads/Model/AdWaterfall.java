package com.segas.tapsellads.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
//water fall parser
public class AdWaterfall {

    //{ "type": "Rewarded", "waterfall": [ { "name": "UnityAds", "id": "TapsellUnityAdsRewarded" }, { "name": "Tapsell", "id": "5caaf03dc1ed8b000149cedd" } ] }


    private  Long WaterfallTime;

    public Long getWaterfallTime() {
        return WaterfallTime;
    }

    public void setWaterfallTime(Long waterfallTime) {
        WaterfallTime = waterfallTime;
    }


    @Expose
    @SerializedName("type")
    public  String type;

    @Expose
    @SerializedName("waterfall")
    private List<Waterfall> waterfalls;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Waterfall> getWaterfalls() {
        return waterfalls;
    }

    public void setWaterfalls(List<Waterfall> waterfalls) {
        this.waterfalls = waterfalls;
    }
}
