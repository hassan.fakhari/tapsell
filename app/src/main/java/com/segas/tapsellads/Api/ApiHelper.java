package com.segas.tapsellads.Api;

import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {


    //Retrofit Object Creator
    public Retrofit create()  {
        HttpLoggingInterceptor httpInseperator  = new HttpLoggingInterceptor();
        httpInseperator.setLevel(HttpLoggingInterceptor.Level.BODY);
        List lists = Arrays.asList(ConnectionSpec.COMPATIBLE_TLS, ConnectionSpec.CLEARTEXT);
        OkHttpClient.Builder client = new OkHttpClient().newBuilder();
       //verfired host name for ssl certificate
        client.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                return true;
            }
        });
        client.addInterceptor(httpInseperator);
        client.connectionSpecs(lists);
        Retrofit retrofit =
                new Retrofit.Builder().addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(client.build())
                        .baseUrl("https://cc442df0-f8cf-41b2-8e95-1d0f74d98118.mock.pstmn.io/tapsell.mock/").build();
        return retrofit;

    }






}