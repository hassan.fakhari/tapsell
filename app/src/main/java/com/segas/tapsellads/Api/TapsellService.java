package com.segas.tapsellads.Api;

import com.segas.tapsellads.Model.AdNetwork;
import com.segas.tapsellads.Model.AdWaterfall;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TapsellService {


    //get handler for ad network
    @GET("adnetworks")
    public Observable<AdNetwork> getAdNetwork();



//get handler for reward
    @GET("rewarded")
    public  Observable<AdWaterfall> getAdReward();


}
