package com.segas.tapsellads;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.segas.tapsellads.Api.ApiHelper;
import com.segas.tapsellads.Model.Ad;
import com.segas.tapsellads.Model.AdNetwork;
import com.segas.tapsellads.Model.AdNetworkEnum;
import com.segas.tapsellads.Model.AdWaterfall;
import com.segas.tapsellads.Model.Interfaces.AdNetworkListener;
import com.segas.tapsellads.Model.Interfaces.AdRewardListener;
import com.segas.tapsellads.Model.Waterfall;
import com.segas.tapsellads.Utils.PrefranceHelper;
import com.segas.tapsellads.ViewModel.TapsellViewModel;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.IUnityAdsShowListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.UnityAdsShowOptions;
import com.unity3d.ads.mediation.IUnityAdsExtendedListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ir.tapsell.sdk.TapsellAdRequestListener;
import ir.tapsell.sdk.TapsellAdRequestOptions;
import ir.tapsell.sdk.TapsellAdShowListener;
import ir.tapsell.sdk.TapsellRewardListener;
import ir.tapsell.sdk.TapsellShowOptions;

//sdk main class
public class Tapsell {

  //FreezShow : زمانی که اقدام برای نمایش اطلاعات میشود هر زمان که یکی از شبکه ها اماده نمایش تبلیغات شود به وسیله این متغییر وضعیت از نمایش شبکه بعدی جلوگیری میشود
  //tapsell key , unity key : کلید های راه اندازی دو شبکه تبلیغات که از وب سرویس دریافت می شود
//unity_is_ready : نگه دارنده وضعیت اماده بودن شبکه یونیتی
    //InitRequest : نگه دارنده وضعیت کل sdk
  private   boolean FreezShow = false;
  private   String TapsellKey;
  private   String UnityKey;
   private boolean unity_is_ready =false;
  private   int InitRequest = 0;








     //درخواست گرفتن اطلاعات از شبکه به وسیله وب سرویس
    public   void RequestNetwork(Application context, LifecycleOwner owner, AdNetworkListener adNetworkListener){
        TapsellViewModel model = new TapsellViewModel(new ApiHelper(),context);
        model.RequestNetwork();
        model.getNetwork.observe(owner, new Observer<AdNetwork>() {
            @Override
            public void onChanged(AdNetwork adNetwork) {


                //کلید ها از شبکه دریافت می شود و در متغییر های temp ذخیره می شود
              try {
                  for (Ad ad : adNetwork.getAdNetwork()) {
                      if (ad.getName().equals("UnityAds")) {
                          UnityKey = ad.getId();
                      }
                      if (ad.getName().equals("Tapsell")) {
                          TapsellKey = ad.getId();
                      }
                  }
              }catch (Exception e){



              }

              //وضعیت شبکه در این متغییر تغییر می کند یعنی شبکه ما در حال راه اندازی اولیه می باشد
                if(InitRequest == 1) {
                    //مقدار دهی اولیه شبکه به وسیله این متد
                    InitRequest = 2;
                    //اگر هیچ کدام از شبکه ها را دریافت نکرد یعنی نمی تواند تبلیغات را شروع کند
                    if (UnityKey.isEmpty() && TapsellKey.isEmpty()) {
                        //پیام لاگ مبنی بر اینکه در این زمان هیچ کدام از شبکه ها اماده نیستن
                        Log.w("Tapsell", "At this Time No one of Networks is not Ready" );
                    }
                    //اگر شبکه یونیتی در دسترس نبود
                    else if (UnityKey.isEmpty()) {
                        PrepareNetwork(context, AdNetworkEnum.TapSell);
                        Log.w("Tapsell", "Tapsell Network is Avaible" );

                    }
                    //اگر شبکه تپسل در دسترس نبود
                    else if (TapsellKey.isEmpty()) {
                        PrepareNetwork(context, AdNetworkEnum.Unityad);
                        Log.w("Tapsell", "Unity Network is Avaible" );

                    }
                    //در صورتی که هیچ کدام از شرط های بالا نبود و هر دو شبکه در دسترس بودند
                    else {
                        PrepareNetwork(context, AdNetworkEnum.All);
                        Log.w("Tapsell", "All Network is Avaible" );


                    }
                }

                //رویداد برای نمایش دریافتی ها از شبکه
                adNetworkListener.NetworkReciver(adNetwork);




            }
        });
    }

    //اماده سازی شبکه
    private  void PrepareNetwork(Application context,AdNetworkEnum adNetworkEnum){
        //شرط راه اندازی شبکه
        if(adNetworkEnum == AdNetworkEnum.TapSell){
            //راه اندازی شبکه تپ سل
            ir.tapsell.sdk.Tapsell.initialize(context,TapsellKey);

        }else if(adNetworkEnum == AdNetworkEnum.Unityad){
            //راه اندازی شبکه یونیتی
            UnityAds.initialize(context,UnityKey);
            UnityAds.addListener(new IUnityAdsListener() {
                @Override
                public void onUnityAdsReady(String s) {

                    //زمانی که شبکه یونیتی اماده بود به وسیله این متغییر اطلاع بده
                    unity_is_ready = true;
                    Log.w("Tapsell", "Unity Network is Ready" );

                }

                @Override
                public void onUnityAdsStart(String s) {
                    Log.w("Tapsell", "Unity Ads Start" );

                }

                @Override
                public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                    Log.w("Tapsell", "Unity Ads Finish" );

                }

                @Override
                public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                    Log.w("Tapsell", "Unity Ads Error " + s );

                }
            });


        }else if(adNetworkEnum == AdNetworkEnum.All){
            ir.tapsell.sdk.Tapsell.initialize(context,TapsellKey);
            UnityAds.initialize(context,UnityKey);
            UnityAds.addListener(new IUnityAdsExtendedListener() {
                @Override
                public void onUnityAdsClick(String s) {
                }

                @Override
                public void onUnityAdsPlacementStateChanged(String s, UnityAds.PlacementState placementState, UnityAds.PlacementState placementState1) {

                }

                @Override
                public void onUnityAdsReady(String s) {
                    unity_is_ready = true;
                    Log.w("Tapsell", "Unity Network is Ready" );

                }

                @Override
                public void onUnityAdsStart(String s) {
                    Log.w("Tapsell", "Unity Ads Start" );

                }

                @Override
                public void onUnityAdsFinish(String s, UnityAds.FinishState finishState) {
                    Log.w("Tapsell", "Unity Ads Finish" );


                }

                @Override
                public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String s) {
                    Log.w("Tapsell", "Unity Ads Error " + s );

                }
            });


        }
    }

    //sdk برای اولین بار مقدار دهی میشود و به وسیله دریافت اطلاعات از وب سرویس در صورتی که هر کدام از شبکه ها در دسترس بود ان را مقدار دهی اولیه می کند
    public void InitializeNetwork(Application context,LifecycleOwner owner,AdNetworkListener networklistener){

         //در شروع برنامه اول درخواستی به تپسل ارسال میشود تا لیست شبکه ها را دریاف کند
          if(InitRequest == 0) {
            InitRequest = 1;
            RequestNetwork(context, owner, networklistener);
            //مقدار دهی اولیه حافظه برای ذخیره کردن
            new PrefranceHelper(context);
           }else{
              //یعنی این متد از قبل راه اندازی شده
              Log.d("Tapsell", "Network Already Initialized");
          }

        }




     // درخواست تبلیغات از وب سرویس
    public  void RequestAd(Activity activity, Context context, LifecycleOwner lifecycle, AdRewardListener adRewardListener){
        AdWaterfall waterfall = null;
        try{
            //اگه واتر فال در حافظه کش ذخیره شده بود ان را برگردان
            waterfall = PrefranceHelper.GetLastWaterFall();

        }catch (Exception e){

        }

        try{

if(waterfall == null) {

    TapsellViewModel model = new TapsellViewModel(new ApiHelper(), context);
    model.RequestReward();
    model.getReward.observe(lifecycle, new Observer<AdWaterfall>() {
        @Override
        public void onChanged(AdWaterfall adWaterfall) {

            adRewardListener.RewardReciver(adWaterfall);
            try {
                PrefranceHelper.AddWaterFall(adWaterfall, Calendar.getInstance().getTime().getTime());
            }catch (Exception e){}




        }
    });


}else{
//بررسی کن ایا از زمان 1 ساعت گذشته یا خیر
    Date date = new Date(waterfall.getWaterfallTime());
    Date Now =  Calendar.getInstance().getTime();
    if(getDifference(Now,date) >=1){

        TapsellViewModel model = new TapsellViewModel(new ApiHelper(), context);
        model.RequestReward();
        model.getReward.observe(lifecycle, new Observer<AdWaterfall>() {
            @Override
            public void onChanged(AdWaterfall adWaterfall) {

                adRewardListener.RewardReciver(adWaterfall);
                try {
                    PrefranceHelper.AddWaterFall(adWaterfall, Calendar.getInstance().getTime().getTime());
                }catch (Exception e){}
            }
        });

    }else{

        adRewardListener.LastReward(waterfall);
    }

}

  }catch (Exception e){

   }
    }

//متد برای محاسبه اختلاف دو تاریخ
    private long getDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
return  different;

    }

    //اماده سازی تبلیغات تپسل
    private  void PrepareAd(Activity activity,Context context,String zoneId){

        ir.tapsell.sdk.Tapsell.requestAd(context, zoneId, new TapsellAdRequestOptions(), new TapsellAdRequestListener() {
            @Override
            public void onError(String s) {
                super.onError(s);
                Log.d("Tapsell", s);

            }

            @Override
            public void onAdAvailable(String s) {
                super.onAdAvailable(s);
                Log.d("Tapsell", "Tapsell ad is Available");
                if(!FreezShow) {
                    ir.tapsell.sdk.Tapsell.showAd(context, zoneId, s, new TapsellShowOptions(), new TapsellAdShowListener() {
                        @Override
                        public void onOpened() {
                            super.onOpened();
                            FreezShow = true;
                            Log.d("Tapsell", "Tapsell is Show");

                        }

                        @Override
                        public void onClosed() {
                            super.onClosed();
                            FreezShow = false;

                        }


                        @Override
                        public void onError(String s) {
                            super.onError(s);
                            FreezShow = false;
                            Log.d("Tapsell", "Tapsell Error Ad :" + s );

                        }

                        @Override
                        public void onRewarded(boolean b) {
                            super.onRewarded(b);
                            FreezShow = false;
                        }
                    });
                }
            }
        });


    }

     //نمایش تبلیغات با اطلاعات قبلی
    public void ShowAd(Activity activity,Context context,String tapsell_zone_Id,String unity_zone_id){
        Log.d("Tapsell", "Start TapsellShowAd " );
         PrepareAd(activity,context,tapsell_zone_Id);

    if(unity_is_ready){
        unity_is_ready = false;
        Log.d("Tapsell", "Start Unity ShowAd " );

        if(!FreezShow) {
            UnityAds.show(activity, unity_zone_id, new IUnityAdsShowListener() {
                @Override
                public void onUnityAdsShowFailure(String s, UnityAds.UnityAdsShowError unityAdsShowError, String s1) {
                    Log.d("Tapsell", "Unity Is Failed " );

                }

                @Override
                public void onUnityAdsShowStart(String s) {
                    Log.d("Tapsell", "Unity Is Start " );

                    FreezShow = true;
                }

                @Override
                public void onUnityAdsShowClick(String s) {

                }

                @Override
                public void onUnityAdsShowComplete(String s, UnityAds.UnityAdsShowCompletionState unityAdsShowCompletionState) {
FreezShow = false;
                    Log.d("Tapsell", "Unity Is Complete " );

                }
            });
        }
}



    }




}
