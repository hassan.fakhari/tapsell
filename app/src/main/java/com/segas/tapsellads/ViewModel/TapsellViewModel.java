package com.segas.tapsellads.ViewModel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.segas.tapsellads.Api.ApiHelper;
import com.segas.tapsellads.Api.TapsellService;
import com.segas.tapsellads.Model.AdNetwork;
import com.segas.tapsellads.Model.AdWaterfall;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.schedulers.Schedulers;
//network connector view model
public class TapsellViewModel extends ViewModel {


    ApiHelper apiHelper;
    Context context;

//network connector live model
   public MutableLiveData<AdNetwork> getNetwork = new MutableLiveData<AdNetwork>();
   public MutableLiveData<AdWaterfall> getReward = new MutableLiveData<AdWaterfall>();


    public  TapsellViewModel(ApiHelper apiHelper, Context context){
        this.apiHelper = apiHelper;
        this.context = context;
    }

    //request  methods
    public  void RequestNetwork(){

        TapsellService api =   apiHelper.create().create(TapsellService.class);
        api.getAdNetwork().observeOn(Schedulers.newThread()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(it->{

            getNetwork.postValue(it);
        }, it->{
            Log.w("www", "RequestAllServer: " + it.getMessage() );
        });
    }

    //request model
public  void RequestReward(){

    TapsellService api =   apiHelper.create().create(TapsellService.class);
    api.getAdReward().observeOn(Schedulers.newThread()).subscribeOn(AndroidSchedulers.mainThread()).subscribe(it->{

        getReward.postValue(it);
    }, it->{
        Log.w("www", "RequestAllServer: " + it.getMessage() );
    });
}








}
